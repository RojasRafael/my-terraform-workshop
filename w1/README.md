# Workshop 1. The first instance

For this workshop we are going to create a first instance on AWS using terraform. We are going to use the `provider.tf' file with the credentials we created on the last workshop. 

1. Log into your AWS console and go to the `us-east-1` region. Go to your VPC dashboard and copy the following data: 
   * Default VPC ID. Each AWS region has a default VPC, copy this ID for this workshop's case. For example, the default VPC for this workshop is: `vpc-03f0f6362bc558fe0`
   * One of the default subnets attached to the default VPC. The default VPC has its subnets, take one and grab its ID. For  example: `subnet-a35e95de`. 
   * The availability zone of the previous subnet. This default subnet must have in its description an availability zone, copy that info too. Like: `us-east-2b`.

2. Also navigate to the EC2 console, click on launch new instance, you will copy some information from the instance creation wizard: 
   * Instance type. As there are [many different instance types on AWS](https://aws.amazon.com/ec2/instance-types/) with different pricing we need to set up the type of instance to launch. Using the free tier of AWS use the; `t2.micro` type.
   * The AMI id: [The Amazon Machine Image](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html). The snapshot that is an image base for the instance. We are using Amazon Linux x86_64 AMI on `us-east-2`: `ami-074cce78125f09d61`.

3. Open the `code/ec2.tf` file and edit the content with the info copied:
   
* The VPC ID on the aws security group creation:
   ```
   resource "aws_security_group" "my_instance_security_group" {
   name        = "myinstance-sg"
   description = "My instance group."
   vpc_id      = "vpc-97aedbfc"
   }

* The subnet and AMI info into the AWS instance "my_instance":
```
  resource "aws_instance" "my_instance" {
    instance_type               = "t2.micro"
    ami                         = "ami-074cce78125f09d61"
    availability_zone           = "us-east-2b"
    subnet_id                   = "subnet-a35e95de"
    vpc_security_group_ids      = ["${aws_security_group.my_instance_security_group.id}"]
    associate_public_ip_address = true
    tags = {
      Name = "my-instance"
    }
  }
```

* The `ec2.tf` file should look close to this:

```
  provider "aws" {
    access_key = "XXXXXXXXXXXXXXXXXXXXXXXXX"
    secret_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    region     = "us-east-2"
  }
    resource "aws_security_group" "my_instance_security_group" {
      name        = "myinstance-sg"
      description = "My instance group."
      vpc_id      = "vpc-03f0f6362bc558fe0"
    }

    resource "aws_security_group_rule" "ssh_ingress_access" {
      type              = "ingress"
      from_port         = 22
      to_port           = 22
      protocol          = "tcp"
      cidr_blocks       = ["0.0.0.0/0"]
      security_group_id = aws_security_group.my_instance_security_group.id
    }

    resource "aws_security_group_rule" "egress_access" {
      type              = "egress"
      from_port         = 0
      to_port           = 65535
      protocol          = "tcp"
      cidr_blocks       = ["0.0.0.0/0"]
      security_group_id = aws_security_group.my_instance_security_group.id
    }

    resource "aws_instance" "my_instance" {
      instance_type               = "t2.micro"
      ami                         = "ami-02e136e904f3da870"
      availability_zone           = "us-east-2b"
      subnet_id                   = "subnet-a35e95de"
      vpc_security_group_ids      = ["${aws_security_group.my_instance_security_group.id}"]
      associate_public_ip_address = true
      tags = {
        Name = "my-instance"
      }
    }
```    
4. Now, save and close the file and initialize terraform on the same directory: `terraform init`.
5. Wht you run `terraform plan` the output will show the the list of resources and their settings. Output is too big and redundant to paste here, but the within the last lines we have the confirmation of the resources that will be created of modified:

`Plan: 4 to add, 0 to change, 0 to destroy.`

5. If you run `terrafom apply` it will create out instance. Again it will prompt you with the changes and as you to type 'yes' or 'no' to apply, tye 'yes':

```Plan: 4 to add, 0 to change, 0 to destroy.

  Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value:yes
  ```
It will prompt out the creation of many resources with their AWS unique indentifier and at the end of it you will see the message: `Apply complete! Resources: 4 added, 0 changed, 0 destroyed.`

If you navigate to your AWS console and the EC2 dashboard you will be able to see your instance here:

![instance](https://i.imgur.com/tJ3WIkG.png)


##### So, so far we:

* Edited a basic `ec2.tf` to get an instance created on terraform. Many of the information used had to be borrowed manually from the AWS console, this is not a common practice and defaults within AWS can be called directly from terrafom. That will be checked later in the workshop. 
* The `ec2.tf` file has the provider AWS credentials on it. This is not a normal practice on terraform and will be taken out later on the workshop. **No AWS credentials go into tf code files**.
* The instance is unreachable (you might noticed just now XD). A set of credentials ato reach the instance will be used later to properly access them. 

#### Cleaning up.

If we leave the resources there they may incur in AWS charges, so it's best practice to clear everything up once we finished the workshop. To clean everything just do: `terraform destroy`.

It will prompt you with how many resources its going to destroy and you have to manually set `yes`:

```
Plan: 0 to add, 0 to change, 4 to destroy.

Do you really want to destroy all resources?
  Terraform will destroy all your managed infrastructure, as shown above.
  There is no undo. Only 'yes' will be accepted to confirm.

  Enter a value: yes
  ```

One you see this message `Destroy complete! Resources: 4 destroyed.` you'll free to go. No AWS resources left to check. 


#### Bibliography:
* [Instance types on AWS](https://aws.amazon.com/ec2/instance-types/)
* [AWS AMIs](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AMIs.html) 
* [aws_instance terraform resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance)
