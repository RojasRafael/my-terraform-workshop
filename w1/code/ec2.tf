  provider "aws" {
    access_key = "XXXXXXXXXXXXXXXXXXXXXXXXX"
    secret_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    region     = "us-east-2"
  }

resource "aws_security_group" "my_instance_security_group" {
  name        = "myinstance-sg"
  description = "My instance group."
  vpc_id      = "vpc-97aedbfc"
}

resource "aws_security_group_rule" "ssh_ingress_access" {
  type              = "ingress"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_instance_security_group.id
}

resource "aws_security_group_rule" "egress_access" {
  type              = "egress"
  from_port         = 0
  to_port           = 65535
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.my_instance_security_group.id
}

resource "aws_instance" "my_instance" {
  instance_type               = "t2.micro"
  ami                         = "ami-074cce78125f09d61"
  availability_zone           = "us-east-2b"
  subnet_id                   = "subnet-a35e95de"
  vpc_security_group_ids      = ["${aws_security_group.my_instance_security_group.id}"]
  associate_public_ip_address = true
  tags = {
    Name = "my-instance"
  }
}