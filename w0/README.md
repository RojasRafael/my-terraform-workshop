# Workshop 0. pre-requisites.

Before we start with terraform we need to setup some tools and credentials. We also need to read and
undestand some core concepts of terraform and aws.


### The pre-requisites:

* A windows system with WSL2 with Ubuntu 20.04 or native Ubuntu running.
* An AWS account, we intend to use the fre tier of AWS, but some workshops might require some AWS charges (don't worry, with care to shutdown resources it will be less than a cup of coffee).
* Terraform v1.0.8. The latest terraform version when writting this guide. For instructions on how to install terraform follow this [guide](https://www.terraform.io/docs/cli/install/apt.html).
* An IAM profile with [the minimal set of permissions to use terraform with AWS](https://github.com/ozbillwang/terraform-best-practices#minimum-aws-permissions-necessary-for-a-terraform-run). **Do not use the AWS root account for terraform**.
* Nice to have/personnal suggestion: VSCode on Windows or Linux.

### Something to read.

Before starting you should read and become familiar with some concepts, please do not *save time* skipping these documents, they will be helpful.

* [What is infrastructure as code](https://www.redhat.com/en/topics/automation/what-is-infrastructure-as-code-iac).
* [Key features and introduction to Terraform.](https://www.terraform.io/intro/index.html)

### First step

1. Create a IAM user with the [terraform minimal permissions](https://github.com/ozbillwang/terraform-best-practices#minimum-aws-permissions-necessary-for-a-terraform-run) set. 
   
2. cd into the code directory of this repo and edit the `provider.tf` file:

```
provider "aws" {
    access_key = "XXXXXXXXXXXXXXXXXxxxxxxxx"
    secret_key = "XXXXXXXXXXXXXXXXXXXXxxxxxxxxxxxxxxxx"
    region     = "us-east-1"
}
```
Where we set the AWS credentials we created for terraform, is not a best practice but for this first workshop we set the `access_key` and `secret_key` as plain text. We also set the default AWS region to work with (us west 2 for this case).

3.  Save and close the `provider` file and inside the same directory on you command line run the following command:

* `terraform init`. This will initialize the terraform provider on your local work environment, it will download and install the latest AWS provider. Output will be something like:

```
Initializing the backend...

Initializing provider plugins...
- Finding latest version of hashicorp/aws...
- Installing hashicorp/aws v3.62.0...
- Installed hashicorp/aws v3.62.0 (signed by HashiCorp)

Terraform has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that Terraform can guarantee to make the same selections by default when
you run "terraform init" in the future.

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

4. Issue other terraform commands, like `terraform -v` to check the version, or `terraform providers` to see the providers installed on your workspace (currently just using AWS).


Done. After finishing this workshop you should be able to use a properly configured provider on your terraform installation (Natuve Ubuntu or on WSL). This provider file will be used along many of these workshops. 