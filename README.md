# My Terraform Workshop


This is a smal terraform workshop to jump from zero to 100 on your terraform hacking. It will use the AWS freetier to work with, however some workshops may or may not incur in usage charges, but don't worry: with good infra management they full cost will be less than a cup of coffee.

It will be divided into modules with each module with its code folder with the examples used for the workshop. 

This workshoos was created and tested under Ubuntu 20.04.3 under WSL2 and with terraform 1.0.8.


### Modules

0. Intial configuration, and presteps. Also with some documentation to read before starting. Setting up the `terraform` provider.